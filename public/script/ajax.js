var AjaxAPI = {}

AjaxAPI.get = function (url,data,success,error){


	var xhr = new XMLHttpRequest()

	xhr.open('get',url)

	xhr.onreadystatechange = function(evt){
		if(xhr.readyState == 4){
			if(xhr.status >= 200 && xhr.status< 300){
				var data = JSON.parse(xhr.responseText)
				success(data)
			}
			else{
				error(xhr.status,xhr.statusText)
			}
		}
	}

	xhr.send();
}