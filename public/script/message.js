"use strict"

class Message{

  constructor(from,to,obj,msg,etat){
      this.from = from;
      this.to = to;
      this.obj = obj;
      this.msg = msg;
      this.etat = etat;
  }

  get from (){ return this.from }
  set from (from){ this.from = from }

  get to (){ return this.to }
  set to (from){ this.to = to }

  get obj (){ return this.obj }
  set obj (from){ this.obj = obj }

  get msg (){ return this.msg }
  set msg (from){ this.msg = msg }

  get etat (){ return this.etat }
  set etat (from){ this.etat = etat }


  register(){
    console.log(this.from+' is now registered');
  }

}

export default Message;


  


