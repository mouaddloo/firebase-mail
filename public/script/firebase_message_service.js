"use strict"

class Message{

  constructor(from,to,obj,msg,etat){
      this.from = from;
      this.to = to;
      this.obj = obj;
      this.msg = msg;
      this.etat = etat;
  }

  register(){
    console.log(this.from+' is now registered');
  }

}


var modfkey 
var FB = {}

FB.send = function (from){
  
            var to = document.getElementById('to').value;
            var obj = document.getElementById('objet').value;
            var msg = document.getElementById('message').value;
            if(document.getElementById('urgent').checked==true){
              var etat = "urgent"
            }
            else{
              var etat = "normal"
            }    
            var m = new Message(from,to,obj,msg,etat);
            firebase.database().ref('messages').push(m)

}

FB.resend = function (from){
  
            var to = document.getElementById('to').value;
            var obj = document.getElementById('objet').value;
            var msg = document.getElementById('message').value;
            if(document.getElementById('urgent').checked==true){
              var etat = "urgent"
            }
            else{
              var etat = "normal"
            }    
            var m = new Message(from,to,obj,msg,etat);
            //firebase.database().ref('messages').push(m)
            firebase.database().ref().child('/messages/' + modfkey).update(m);

}

FB.inbox = function (usermail){

            var table = document.querySelector("tbody");
            firebase.database().ref('messages').on('child_added', function(data) {
                  var m = new Message(data.val().from,data.val().to,data.val().obj,data.val().msg,data.val().etat);
                  var mailuser = document.getElementById('usermail').innerText
                  if(data.val().to == mailuser ){
                        table.appendChild(creerLigne(data.key,m.from,m.obj,m.etat,m.msg,"inbox"))
                  }
            });
            firebase.database().ref('messages').on('child_changed', function(data) {
            });
            firebase.database().ref('messages').on('child_removed', function(data) {
            });

}

FB.sentmail = function (usermail){

            var table = document.querySelector("tbody");
            firebase.database().ref('messages').on('child_added', function(data) {
                var m = new Message(data.val().from,data.val().to,data.val().obj,data.val().msg,data.val().etat);
                var mailuser = document.getElementById('usermail').innerText
                if(m.from == mailuser ){
                  table.appendChild(creerLigne(data.key,m.to,m.obj,m.etat,m.msg,"sentmail"))
                }
            });
            firebase.database().ref('messages').on('child_changed', function(data) {
              var m = new Message(data.val().from,data.val().to,data.val().obj,data.val().msg,data.val().etat);
              console.log(data.key)
              var tr = document.getElementById(data.key)
              var items = tr.getElementsByTagName('td')
              for (var i = 1; i <= items.length; i++) {
                  if(i==1) items[i].innerHTML = m.to ;
                  if(i==2) items[i].innerHTML = m.obj ;
                  if(i==3) items[i].innerHTML = m.etat ;
                  if(i==5) items[i].innerHTML = m.msg ;
              }
            });
            firebase.database().ref('messages').on('child_removed', function(data) {
            });
            
}


function creerLigne(key,from, obj, etat,msg,type){

            var tr = document.createElement("tr");
            tr.id = key

            var td0 = document.createElement("td");
            var td1 = document.createElement("td");
            var td2 = document.createElement("td");
            var td3 = document.createElement("td");
            var td4 = document.createElement("td");
            var td5 = document.createElement("td");
            td5.className='hide';
            
            var ttd0 = document.createElement("input");
            ttd0.type="checkbox";
            ttd0.name="ncheck";

            var ttd1 = document.createTextNode(from);
            var ttd2 = document.createTextNode(obj);
            var ttd5 = document.createTextNode(msg);

            td0.appendChild(ttd0);
            td1.appendChild(ttd1);
            td2.appendChild(ttd2);
            td5.appendChild(ttd5);

            if(etat=="normal"){
            td3.innerHTML="<span class='label label-info'>Normal</span>"
            }if(etat=="urgent"){
            td3.innerHTML="<span class='label label-danger'>Urgent</span>"
            }

            if(type=="inbox"){
            td4.innerHTML="<button type='button' class='btn btn-info' id ='modal' data-toggle='modal' data-target='#myModal'>show <span class='glyphicon glyphicon-envelope'></span></button>"
            }
            else{
            td4.innerHTML="<button type='button' class='btn btn-info' id ='modal' data-toggle='modal' data-target='#myModal'>update <span class='glyphicon glyphicon-refresh'></span></button>"
            }

            tr.appendChild(td0);
            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);
            tr.appendChild(td4);
            tr.appendChild(td5);

            return(tr);
}



var im = document.querySelector("#modalsend");
im.addEventListener("click", initmodal);

var de = document.querySelector("#delete");
de.addEventListener("click", supprimerLesLignesSelectionnees);


var test 

function showmessage(event){
  var targetElement = event.target || event.srcElement;
        console.log(targetElement);
        test = targetElement;
        
        if(event.target instanceof HTMLButtonElement){
          document.getElementById('to_from').innerHTML = 'from'
          document.getElementById('urgentp').style.display = 'none'
          document.getElementById('send').style.display = 'none'
          var from = targetElement.parentNode.parentNode.cells[1].innerHTML
          var f = document.getElementById('to')
          f.value=from;
          f.disabled = true;
          var object = targetElement.parentNode.parentNode.cells[2].innerHTML
          var o = document.getElementById('objet')
          o.value=object;
          o.disabled = true;
          var msg = targetElement.parentNode.parentNode.cells[5].innerHTML
          var m = document.getElementById('message')
          m.value=msg;
          m.disabled = true; 
    }
}

function updatemessage(event){
  var targetElement = event.target || event.srcElement;
        console.log(targetElement);
        test = targetElement;
        
        if(event.target instanceof HTMLButtonElement){
          document.getElementById('to_from').innerHTML = 'to'
          document.getElementById('urgentp').style.display = 'block'
          document.getElementById('send').innerHTML = 'resend'
          var from = targetElement.parentNode.parentNode.cells[1].innerHTML
          var f = document.getElementById('to')
          f.value=from;
          f.disabled = false;
          var object = targetElement.parentNode.parentNode.cells[2].innerHTML
          var o = document.getElementById('objet')
          o.value=object;
          o.disabled = false;
          var msg = targetElement.parentNode.parentNode.cells[5].innerHTML
          var m = document.getElementById('message')
          m.value=msg;
          m.disabled = false; 
          var modf = document.getElementById('modify')
          //modf.value 

          modfkey=targetElement.parentNode.parentNode.id
          console.log(modfkey)
    }
}

function initmodal(){
          document.getElementById('to_from').innerHTML = 'to'
          document.getElementById('urgentp').style.display = 'block'
          document.getElementById('send').innerHTML = 'send'
          document.getElementById('send').style.display = 'block'
          document.getElementById("to").disabled = false
          document.getElementById("to").value = ''
          document.getElementById("objet").disabled = false
          document.getElementById("objet").value = ''
          document.getElementById("message").disabled = false
          document.getElementById("message").value = ''
}


var select = document.querySelector('input[id="selectAll"]');
select.addEventListener("click", function(){
       var se=document.getElementsByName('ncheck');
          for(var i=0;i<se.length;i++){
        if(se[i].checked){
          se[i].checked=false;
        }
        else{
          se[i].checked=true;
        }
      }
    });


function supprimerLesLignesSelectionnees(){
    var sall = document.querySelectorAll('input[name="ncheck"]');
    console.log(sall);
    for(var i=0;i<sall.length;i++){
      if(sall[i].checked){
        sall[i].parentNode.parentNode.parentNode.removeChild(sall[i].parentNode.parentNode);
        firebase.database().ref('messages/'+sall[i].parentNode.parentNode.id).remove();
      }
    }
}